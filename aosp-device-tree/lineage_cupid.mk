#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from cupid device
$(call inherit-product, device/xiaomi/cupid/device.mk)

PRODUCT_DEVICE := cupid
PRODUCT_NAME := lineage_cupid
PRODUCT_BRAND := Xiaomi
PRODUCT_MODEL := 2201123G
PRODUCT_MANUFACTURER := xiaomi

PRODUCT_GMS_CLIENTID_BASE := android-xiaomi

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="lineage_cupid-userdebug 13 TQ3A.230805.001 eng.cristi.20230812.222754 test-keys"

BUILD_FINGERPRINT := Xiaomi/lineage_cupid/cupid:13/TQ3A.230805.001/cristian08122226:userdebug/test-keys
